% This script converts DIC (digital image correlation) files from NCORR to
% the format of VIC-2D. 
% This is an essential pre-processing step when running ACDM 2.0
% (https://gitlab.ethz.ch/ibk-kfm-public/acdm/-/releases/v2.0) on NCORR
% files. In ACDM 2.0, the converted files can then be read as VIC-2D DIC
% files.
%
% Other m-files required: none
% Subfunctions: principalStrain
% MAT-files required: none
%
% Author: Nicola Gehri, Doctoral student, Group Prof. Dr. Walter Kaufmann
% Institute of Structural Engineering, ETH Zurich
% email address: ngehri@ethz.ch  
% Website: hhttps://kaufmann.ibk.ethz.ch/
% October 2021; Last revision: 29-October-2021

%------------- BEGIN CODE --------------
clc
clear all

% UI select NCORR file
[file,path] = uigetfile('*.mat');
if isequal(file,0)
    return
end

% load NCORR file
f = waitbar(0.5,'Loading file...','Name','Please wait');
load(fullfile(path,file))
close(f)

% initialisation
number_files = length(current_save)+1;

file_names = string(3);
mat_files = struct(...
    'e1',[],...
    'gamma',[],...
    'sigma',[],...
    'x',[],...
    'y',[],...
    'u',[],...
    'v',[],...
    'x_c',[],...
    'y_c',[],...
    'u_c',[],...
    'v_c',[]);

try
    % get file names
    for i = 1:length(current_save)
        [~,file_names(i+1),~] = fileparts(current_save(i).name);
    end
    [~,file_names(1),~] = fileparts(reference_save.name);
catch
    msgbox('An error occured. File conversion was not successul.','Error','error')
    return
end



% UI confirm writing files

answer = questdlg(["The following files will be written in:",string([fullfile(path),'convertedFiles']),"",file_names], ...
    'Confirm...', ...
    'Ok','Cancel','Ok');
% Handle response
switch answer
    case 'Cancel'
        return
end

%% Convert files
f = waitbar(0,'Please wait...','Name','Writing files...');

try
    % dic properties
    spacing = data_dic_save.dispinfo.spacing+1; % Note: +1 in the definition
    
    % current files
    for i = 1:length(current_save)
        [m,n] = size(data_dic_save.displacements(i).plot_u_dic);
        
        
        mat_files(i+1).x = repmat([1:n],m,1).*spacing;
        mat_files(i+1).y = repmat([1:m]',1,n).*spacing;
        mat_files(i+1).x_c = mat_files(i+1).x*data_dic_save.dispinfo.pixtounits;
        mat_files(i+1).y_c = -mat_files(i+1).y*data_dic_save.dispinfo.pixtounits; %right coordinate system
        
        mat_files(i+1).u = data_dic_save.displacements(i).plot_u_dic;
        mat_files(i+1).v = data_dic_save.displacements(i).plot_v_dic;
        mat_files(i+1).u_c = data_dic_save.displacements(i).plot_u_ref_formatted;
        mat_files(i+1).v_c = -data_dic_save.displacements(i).plot_v_ref_formatted; %right coordinate system
        
        mat_files(i+1).sigma = (data_dic_save.displacements(i).plot_corrcoef_dic > 0)*2-1;
        condition = data_dic_save.displacements(i).plot_corrcoef_dic > data_dic_save.dispinfo.cutoff_corrcoef(i);
        mat_files(i+1).sigma(condition) = -1;
        
        condition = and(data_dic_save.displacements(i).plot_corrcoef_dic <= data_dic_save.dispinfo.cutoff_corrcoef(i),data_dic_save.displacements(i).plot_corrcoef_dic > 0);
        mat_files(i+1).sigma(condition) = data_dic_save.displacements(i).plot_corrcoef_dic(condition);
        
        
        exx = data_dic_save.strains(i).plot_exx_ref_formatted;
        exy = data_dic_save.strains(i).plot_exy_ref_formatted;
        eyy = data_dic_save.strains(i).plot_eyy_ref_formatted;
        [e1,gamma] = principalStrain(exx,eyy,exy);
        
        
        mat_files(i+1).e1 = e1;
        mat_files(i+1).gamma = -gamma;
    end
    
    % reference image
    mat_files(1).x = mat_files(2).x;
    mat_files(1).y = mat_files(2).y;
    mat_files(1).u = mat_files(2).u*0;
    mat_files(1).v = mat_files(2).v*0;
    mat_files(1).x_c = mat_files(2).x_c;
    mat_files(1).y_c = mat_files(2).y_c;
    mat_files(1).u_c = mat_files(2).u_c*0;
    mat_files(1).v_c = mat_files(2).v_c*0;
    mat_files(1).e1 = mat_files(2).e1*0;
    mat_files(1).sigma= mat_files(2).sigma;
    mat_files(1).gamma = mat_files(2).gamma*0;
    
    waitbar(0.1,f,[sprintf('%.1f',10),'%'])
    
    
    cd(path)
    mkdir convertedFiles
    cd convertedFiles
    
    
    %save files
    for i = 1:length(current_save)+1
        temp_mat_file = mat_files(i);
        save([char(file_names(i)),'.mat'],'-struct','temp_mat_file')
        waitbar(i/(length(current_save)+1),f,[sprintf('%.1f',i/(length(current_save)+1)*100),'%'])
    end
catch
    rethrow(ME)
    close(f)
    msgbox({'An error occured. File conversion was not successul.','','Information:','Ensure in the NCORR analysis that strains are calculated and the displacements are formatted.'},'Error','error')
    return
end

close(f)
msgbox('Conversion succussful.','Complete','help')
clear all

%% functions
function [e_1,theta_1] = principalStrain(e_x,e_y,e_xy)
%principalStrain - Calculates the major principal strain magnitude and direction of a strain tensor

% Inputs:
%    e_x - normal strain in x-direction [-]
%    e_y - normal strain in y-direction [-]
%    e_xy - shear strain in the xy coordinate system [-]
%
% Outputs:
%    e_1 - major principal strain magnitude [-]
%    theta_1 - major principal strain direction [rad] (in a positive-orientated - also called right-handed orientated - coordinate system: counter-clockwise
%    angle with respect to the x-axis
%
%                      y
%                      ˄
%                      |
% positive-orientated: |
%                      |
%                      - - - - > x
%
%                      
%                      - - - - > x
%                      |
% neagtive-orientated: |
%                      |
%                      ˅
%                      y
%
%
%
e_1 = (e_x+e_y)./2+(((e_x-e_y)./2).^2+(e_xy).^2).^0.5;
theta_1 = atan2(2*e_xy,e_x-e_y)/2;
end

%------------- END OF CODE --------------


