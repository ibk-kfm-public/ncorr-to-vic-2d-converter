# NCORR to VIC-2D converter

## Description
The MATLAB script *src/NCORR_to_VIC2D_converter.m* converts the DIC (digital image correlation) results generated with the free and open-source [NCORR](http://www.ncorr.com/)  software to the format of the commercial DIC software [VIC-2D](https://www.correlatedsolutions.com/vic-2d/) from Correlated Solutions. 

This is an essential pre-processing step when running [ACDM 2.0](https://gitlab.ethz.ch/ibk-kfm-public/acdm/-/releases/v2.0) (automatic crack detection and measurement) analysis on NCORR files. In ACDM 2.0, the converted files can then be read as VIC-2D DIC files.

In combination with NCORR, ACDM offers an end-to-end free and open-source solution to measure cracks in experiments based on digital image correlation.

## Prerequisites
In the NCORR analyis, ensure that 
-   The displacements are formatted:<br>
    <img src="assets/images/Format Displacements.PNG"  width="400">

-   Strains are calculated:<br>
    <img src="assets/images/Calculate Strains.PNG"  width="400">

-   The NCORR files are saved (this is the file to be converted):<br>
    <img src="assets/images/save.PNG"  width="400">


## Usage
1.  Run *NCORR_to_VIC2D_converter.m* in a MATLAB instance
    ```bash
    >> NCORR_to_VIC2D_converter
    ```
2.  Select the NCORR file to be converted.
3.  A new folder named *convertedFiles* is created in the directory of the NCORR file. In this folder, the DIC files are saved in VIC-2D format, which can then be read in [ACDM 2.0](https://gitlab.ethz.ch/ibk-kfm-public/acdm/-/releases/v2.0).

## Author
This converter is developed by [Nicola Gehri](https://kaufmann.ibk.ethz.ch/people/staff/nicola-gehri.html) at the [Chair of Structural Engineering - Concrete Structures and Bridge Design, ETH Zürich](https://kaufmann.ibk.ethz.ch/)

## Copyright and license
The MATLAB script is licensed under the [Apache License, Version 2.0](http://www.apache.org/licenses/LICENSE-2.0) (the "License")